import React from 'react';
import { useForm } from "react-hook-form";


/* <pre>{JSON.stringify(userInfo,undefined,2) }</pre> */

const RegisterForm = () => {
  const { register, handleSubmit, formState: { errors } , reset  } = useForm();
  // const handleRegistration = (data) => console.log(data) 
  const handleRegistration = (data) => {
    console.log(data);
    reset();
    alert("Registered Succesfully!!!")
  }

  const handleError = (errors) => {};
  
//   const [formData , setformData] = useState({
//     //   isAgree: true ,//checkbox
//     //   gender : " "//radio
     
//   })
// 
  const registerOptions = {
    
    name: { required: " Name is required" },
    lname: { required: " Last Name is required" },
    email: { required: " Email is required" },
    password: {
      required: " Password is required",
      minLength: {
        value: 6,
        message: " Password must have at least 6 characters"
      }
    },
    phoneNumber: {
      required: " Phone Number is required",
      minLength: {
        value: 10,
        message: "Phone number must have at least 10 characters"
      }
    },
    age : {required: "Age is required"}
    
  };
  

  return (
      <div className='box'> <h2 className='t1'>Welcome to the Registration Form!!!</h2>
    <form onSubmit={handleSubmit(handleRegistration, handleError)}>
      <div className='N1'>
        <label>Name </label>
        <input name="name" type="text" {...register('name', registerOptions.name) }/>
        <small className='error'>
          {errors?.name && errors.name.message}
        </small>
      </div>
      <div className='LN1'>
        <label>Last Name </label>
        <input name="lname" type="text" {...register('lname' , registerOptions.lname)}/>
        <small className='error' >
          {errors?.name && errors.lname.message}
        </small>
      </div>
      <div className='E1'>
        <label>Email </label>
        <input
          type="email"
          name="email"
          {...register('email', registerOptions.email)}
        />
        <small className='error' >
          {errors?.email && errors.email.message}
        </small>
      </div>
      <div className='P1'>
        <label>Password </label>
        <input
          type="password"
          name="password"
          {...register('password', registerOptions.password)}
        />
        <small className='error'>
          {errors?.password && errors.password.message}
        </small>
      </div>
      <div className='PN1'>
        <label>Phone Number </label>
        <input
          type="phoneNumber"
          name="phonenumber"
          {...register('phoneNumber', registerOptions.phoneNumber)}
        />
        <small className='error'>
          {errors?.phoneNumber && errors.phoneNumber.message}
        </small>
      </div>

      <div className='A1'>
        <label>Age </label>
        <input
          type="number"
          name="age"
          {...register('age', registerOptions.age)}
        />
        <small className='error'>
          {errors?.age && errors.age.message}
        </small>
      </div>


        <div className='G1'>
         <label>Gender: </label>
         <input name='Male' {...register('checkbox',{required:true})}
          type='radio' value='Male' />
         <label>Male</label>
         <input name='Female' {...register('checkbox',{required:true})} 
         type='radio' value='Female' />
         <label>Female</label>
      
         <div className='T1'>
         <label>Hobbies :</label>
         <select name='Hobby' {...register ('Hobby', {required:true})}>
         <option value='Dance'>Dance</option>
         <option value='Singing'>Singing</option>
         <option value='Reading'>Reading</option>
         <option value='Cooking'>Cooking</option>
         </select>
         </div>
         
         </div>
      <div className='C1'>
          <input type="checkbox" name="isAgree" />
          <label>Agree with the terms and conditions</label>
      </div>

      <button className='B1'>Submit</button>
    </form></div>
  );
};

export default RegisterForm