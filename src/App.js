import './App.css';
import RegisterForm from './RegisterForm';
import ReactGA from 'react-ga';

  const TRACKING_ID = "UA-226089871-2"; // OUR_TRACKING_ID
  
  ReactGA.initialize(TRACKING_ID);

function App() {
  return (
    <div className="App">
      <RegisterForm/>
    </div>
  );
}

export default App;
